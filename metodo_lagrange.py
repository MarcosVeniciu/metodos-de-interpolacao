def problema():

    pontos_X = [-1, 0, 2]
    pontos_Fx = [4, 1, -1]

    n = 2 # são tres elementos onde o indice vai de 0 a n

    X = 1 # -159 é o numero padrão que indica que não ira calcular o valor em um ponto especifico, ira apenas encontrar o polinomio

    solucao = metodo_lagrange(pontos_X, pontos_Fx, n, X)

    exibicao(pontos_X, pontos_Fx, n, X, solucao)



def metodo_lagrange(X, F_x, n, valor):

    solucao = 0
    dividendo = 1
    divisor = 1


    for L in range(n+1):
        dividendo = 1
        divisor = 1
        for indice in range(n+1):
            if indice != L:
                dividendo = dividendo * (valor - X[indice])
                divisor = divisor * (X[L] - X[indice])

        solucao = solucao + ( F_x[L] * (dividendo/divisor))

    return solucao


def exibicao(pontos_X, pontos_Fx, n, X, solucao):

    print("  ------------------------------------ Método de Lagrange ------------------------------------")
    print("")
    print("")
    print("")
    print("     X | ", end="")
    for indice in range(len(pontos_X)):
        print("{0:10.6f}    ".format(pontos_X[indice]), end="")
    print("")
    print("  ------------------------------------------------")
    print("  F(x) | ", end="")
    for indice in range(len(pontos_X)):
        print("{0:10.6f}    ".format(pontos_Fx[indice]), end="")
    print("")
    print("")
    print("")
    print("")
    if X != -159:
        print("  P_{0}({1}) = {2:10.6f}".format(n, X, solucao))
    print("")
    print("")
