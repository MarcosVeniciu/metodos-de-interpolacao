import copy

def problema():

    pontos_X = [ 1, 4, 6, 8] #0.2, 0.34, 0.4, 0.52, 0.6, 0.72
    pontos_Fx = [ 0.693, 2.079, 2.485, 2.773] # 0.16, 0.22, 0.27, 0.29, 0.32, 0.37


    tabela_diferenca = metodo_newton(pontos_X, pontos_Fx)

    exibir(pontos_X, pontos_Fx, tabela_diferenca)



def metodo_newton(X, F_x):
    tabela = []
    linha_tabela = []
    ordem = len(X)

    # cria uma tabela com -99999.99999 para ser ignorada na hora de exibir a tabela
    for linha in range(ordem):
        for coluna in range(ordem+1):
            linha_tabela.append(-99999.99999)
        tabela.append(copy.copy(linha_tabela))
        linha_tabela.clear()

    # cria a tabela de diferença dividida

    # primeira e segunda coluna valor de x e F(x) respectivamente

    for linha in range(ordem):
        tabela[linha][0] = X[linha]
        tabela[linha][1] = F_x[linha]

    # demais colunas da tabela
    for coluna in range(2, ordem+1):
        cont = 0
        print("Ordem {0}".format(coluna-1))
        for linha in range(coluna-1, ordem):

            tabela[linha][coluna] = ( tabela[linha][coluna-1] - tabela[linha-1][coluna-1] )  / ( tabela[linha][0] - tabela[cont][0] )
            print("  ({0:^10.3f}-{1:^10.3f}) / ({2:^10.3f}-{3:^10.3f}) = {4:10.3f}".format(tabela[linha][coluna-1], tabela[linha-1][coluna-1], tabela[linha][0], tabela[cont][0], tabela[linha][coluna]))
            cont = cont +1

    print("")
    return tabela


def exibir(pontos_X, pontos_Fx, tabela_diferenca):

    print("  ------------------------------------ Método de Newton ------------------------------------")
    print("")
    print("")
    print("")
    print("     X | ", end="")
    for indice in range(len(pontos_X)):
        print("{0:10.3f}    ".format(pontos_X[indice]), end="")
    print("")
    print("  -------", end="")
    for indice in range(len(pontos_X)):
        print("{0}".format("--------------"),end="")
    print("")
    print("  F(x) | ", end="")
    for indice in range(len(pontos_X)):
        print("{0:10.3f}    ".format(pontos_Fx[indice]), end="")
    print("")
    print("")
    print("")
    print("     Tabela de diferença dividida ")
    print("")
    print("  {0:^10} ".format("X"), end="")
    for indice in range(len(pontos_X)):
        print("  Ordem {0}   ".format(indice), end="")
    print("")

    print("  ----------", end="")
    for indice in range(len(pontos_X)):
        print("{0}".format("------------"), end="")
    print("")

    for linha in range(len(tabela_diferenca)):
        #linha dos numeros da tabela
        for coluna in range(len(tabela_diferenca[0])):
            if tabela_diferenca[linha][coluna] != -99999.99999:
                if coluna == 0:
                    print("  {0:^10.3f}".format(tabela_diferenca[linha][coluna]), end="")
                else:
                    print("{0:^12.3f}".format(tabela_diferenca[linha][coluna]), end="")
            else:
                print("{0:^12}".format(" "),end="")
        print("")

        # linha com um separador
        for coluna in range(len(tabela_diferenca[0])):
            if coluna == 0:
                print("{0}".format("  ----------"), end="")
            else:
                print("{0}".format("------------"), end="")
        print("")






















    print("")
